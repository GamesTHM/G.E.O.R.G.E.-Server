reines Wissensparkplatzprojekt

### NPM:
location: /usr/lib/node_modules/  
alle NPM Module update (global)  
npm update -g  
  
installiert global eine bestimmte Version des Pakets parse-server  
npm i -g parse-server@2.2.22  
  
### systemctl/systemd:
location: /etc/systemd/system/  
alle Startskripte oder Links auf diese liegen an dieser Stelle  
ACHTUNG: die Parse Skripte liegen in /home/parse/  
parse-dash.service & parse.service  
  
systemctl status *.service --> gibt Infos zum Service aus wie, Laufzeit, die letzten Logzeilen, usw.  
systemctl stop *.service --> fährt den Service herunter, der Parse-Server wartet auf die Beendigung aller Verbindungen (dauert also ewig, kill über htop is einfacher)  
systemctl start *.service --> startet den Dienst  
systemctl restart *.service --> startet den Dienst neu  
  
### Cronjobs:
alle Cronjobs gehören dem Nutzer root  
crontab -l zeigt diese an  
crontab -e lässt sie editieren  
  
### Logs:
Parse-Server:  
/logs/ & /home/parse/logs/  
keiner weiß so genau, warum er sich hin und wieder für die zweite Location entscheidet  
Achtung: Größe im Auge behalten, hin und wieder mal aufräumen.  
  
3S zyklische Jobs:  
/home/parse/cronlogs/  
hourly & daily für die jeweiligen CloudCode Jobs, Punkte Decay usw.  